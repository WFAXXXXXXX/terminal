import * as index from '../src/index.js';
import Terminal from '../src/Terminal.js';
import { expect } from 'chai';

describe( 'package', () => {
	it( 'has only default export', () => {
		expect( index ).to.have.all.keys( 'default' );
	} );

	it( 'exposes Terminal as default', () => {
		expect( index.default ).to.equal( Terminal );
	} );
} );
