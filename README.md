# @chammy/terminal

[![pipeline status](https://gitlab.com/chammyjs/terminal/badges/master/pipeline.svg)](https://gitlab.com/chammyjs/terminal/commits/master)
[![coverage report](https://gitlab.com/chammyjs/terminal/badges/master/coverage.svg)](https://gitlab.com/chammyjs/terminal/commits/master)

Terminal layer for Chammy project.